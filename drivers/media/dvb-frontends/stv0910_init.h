/*
 * Driver for the ST STV0910 DVB-S/S2 demodulator.
 *
 * Copyright (C) 2014-2015 Ralph Metzler <rjkm@metzlerbros.de>
 *                         Marcus Metzler <mocm@metzlerbros.de>
 *                         developed for Digital Devices GmbH
 * Copyright (C) 2015-2016 Chris Lee <updatelee@gmail.com>
 * Copyright (C) 2016      Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 only, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA
 * Or, point your browser to http://www.gnu.org/copyleft/gpl.html
 */

#ifndef __STV0910_INIT_H__
#define __STV0910_INIT_H__

static const enum fe_code_rate s_fec_to_user[] = {
	FEC_NONE,	//	 0
	FEC_NONE,	//	 1
	FEC_NONE,	//	 2
	FEC_NONE,	//	 3
	FEC_NONE,	//	 4
	FEC_NONE,	//	 5
	FEC_NONE,	//	 6
	FEC_NONE,	//	 7
	FEC_NONE,	//	 8
	FEC_NONE,	//	 9
	FEC_NONE,	//	 a
	FEC_NONE,	//	 b
	FEC_NONE,	//	 c
	FEC_1_2,	//	 d
	FEC_NONE,	//	 e
	FEC_NONE,	//	 f
	FEC_NONE,	//	10
	FEC_NONE,	//	11
	FEC_2_3,	//	12
	FEC_NONE,	//	13
	FEC_NONE,	//	14
	FEC_3_4,	//	15
	FEC_NONE,	//	16
	FEC_NONE,	//	17
	FEC_5_6,	//	18
	FEC_6_7,	//	19
	FEC_7_8,	//	1a
	FEC_NONE,	//	1b
	FEC_NONE,	//	1c
	FEC_NONE,	//	1d
	FEC_NONE,	//	1e
	FEC_NONE,	//	1f
};

static const enum fe_code_rate s2_modcode_to_user[] = {
	FEC_NONE,	//	 0
	FEC_1_4,	//	 1
	FEC_1_3,	//	 2
	FEC_2_5,	//	 3
	FEC_1_2,	//	 4
	FEC_3_5,	//	 5
	FEC_2_3,	//	 6
	FEC_3_4,	//	 7
	FEC_4_5,	//	 8
	FEC_5_6,	//	 9
	FEC_8_9,	//	 a
	FEC_9_10,	//	 b
	FEC_3_5,	//	 c
	FEC_2_3,	//	 d
	FEC_3_4,	//	 e
	FEC_5_6,	//	 f
	FEC_8_9,	//	10
	FEC_9_10,	//	11
	FEC_2_3,	//	12
	FEC_3_4,	//	13
	FEC_4_5,	//	14
	FEC_5_6,	//	15
	FEC_8_9,	//	16
	FEC_9_10,	//	17
	FEC_3_4,	//	18
	FEC_4_5,	//	19
	FEC_5_6,	//	1a
	FEC_8_9,	//	1b
	FEC_9_10,	//	1c
	FEC_NONE,	//	1d
	FEC_NONE,	//	1e
	FEC_NONE	//	1f
};

static const u32 s12_frame_length[][3] = {
	{    0,     0,            0}, //  0
	{16200,  3240,            0}, //  1  QPSK_1_4
	{21600,  5400,            0}, //  2  QPSK_1_3
	{25920,  6480,            0}, //  3  QPSK_2_5
	{32400,  7200,            0}, //  4  QPSK_1_2
	{38880,  9720,            0}, //  5  QPSK_3_5
	{43200, 10800,            0}, //  6  QPSK_2_3
	{48600, 11880,            0}, //  7  QPSK_3_4
	{51840, 12600,            0}, //  8  QPSK_4_5
	{54000, 13320,            0}, //  9  QPSK_5_6
	{57600, 14400,            0}, //  a  QPSK_8_9
	{58320,     0,            0}, //  b  QPSK_9_10
	{38880,  9720,            0}, //  c  8PSK_3_5
	{43200, 10800, 5376 * 1 / 2}, //  d  8PSK_2_3	FEC_1_2
	{48600, 11880,            0}, //  e  8PSK_3_4
	{54000, 13320,            0}, //  f  8PSK_5_6
	{57600, 14400,            0}, // 10  8PSK_8_9
	{58320,     0,            0}, // 11  8PSK_9_10
	{43200, 10800, 5376 * 2 / 3}, // 12 16APSK_2_3	FEC_2_3
	{48600, 11880,            0}, // 13 16APSK_3_4
	{51840, 12600,            0}, // 14 16APSK_4_5
	{54000, 13320, 5376 * 3 / 4}, // 15 16APSK_5_6	FEC_3_4
	{57600, 14400,            0}, // 16 16APSK_8_9
	{58320,     0,            0}, // 17 16APSK_9_10
	{48600, 11880, 5376 * 5 / 6}, // 18 32APSK_3_4	FEC_5_6
	{51840, 12600, 5376 * 6 / 7}, // 19 32APSK_4_5	FEC_6_7
	{54000, 13320, 5376 * 7 / 8}, // 1a 32APSK_5_6	FEC_7_8
	{57600, 14400,            0}, // 1b 32APSK_8_9
	{58320,     0,            0}, // 1c 32APSK_9_10
	{    0,     0,            0}, // 1d
	{    0,     0,            0}, // 1e
	{    0,     0,            0}, // 1f
};

static const enum fe_rolloff roll_to_user[] = {
	ROLLOFF_35,
	ROLLOFF_25,
	ROLLOFF_20,
	ROLLOFF_15
};

static const enum fe_modulation mod_to_user[] = {
	MOD_UNKNOWN,	//	 0	
	QPSK,			//	 1
	QPSK,			//	 2
	QPSK,			//	 3
	QPSK,			//	 4
	QPSK,			//	 5
	QPSK,			//	 6
	QPSK,			//	 7
	QPSK,			//	 8
	QPSK,			//	 9
	QPSK,			//	10
	QPSK,			//	11
	PSK_8,			//	12
	PSK_8,			//	13
	PSK_8,			//	14
	PSK_8,			//	15
	PSK_8,			//	16
	PSK_8,			//	17
	APSK_16,		//	18
	APSK_16,		//	19
	APSK_16,		//	20
	APSK_16,		//	21
	APSK_16,		//	22
	APSK_16,		//	23
	APSK_32,		//	24
	APSK_32,		//	25
	APSK_32,		//	26
	APSK_32,		//	27
	APSK_32,		//	28
	MOD_UNKNOWN,	//	29
	MOD_UNKNOWN,	//	30
	MOD_UNKNOWN,	//	31
};

#define Scale10(db, raw)	{ (((db) * 1024) + 5 ) / 10,  raw }
#define Scale100(db, raw)	{ (((db) * 1024) + 50) / 100, raw }

static const struct LookupTable s1_cnr_lookup[] = {
	Scale10(   -5,  65536  ),
	Scale10(    0,   9242  ),  /*C/N=  0dB*/
	Scale10(    5,   9105  ),  /*C/N=0.5dB*/
	Scale10(   10,   8950  ),  /*C/N=1.0dB*/
	Scale10(   15,   8780  ),  /*C/N=1.5dB*/
	Scale10(   20,   8566  ),  /*C/N=2.0dB*/
	Scale10(   25,   8366  ),  /*C/N=2.5dB*/
	Scale10(   30,   8146  ),  /*C/N=3.0dB*/
	Scale10(   35,   7908  ),  /*C/N=3.5dB*/
	Scale10(   40,   7666  ),  /*C/N=4.0dB*/
	Scale10(   45,   7405  ),  /*C/N=4.5dB*/
	Scale10(   50,   7136  ),  /*C/N=5.0dB*/
	Scale10(   55,   6861  ),  /*C/N=5.5dB*/
	Scale10(   60,   6576  ),  /*C/N=6.0dB*/
	Scale10(   65,   6330  ),  /*C/N=6.5dB*/
	Scale10(   70,   6048  ),  /*C/N=7.0dB*/
	Scale10(   75,   5768  ),  /*C/N=7.5dB*/
	Scale10(   80,   5492  ),  /*C/N=8.0dB*/
	Scale10(   85,   5224  ),  /*C/N=8.5dB*/
	Scale10(   90,   4959  ),  /*C/N=9.0dB*/
	Scale10(   95,   4709  ),  /*C/N=9.5dB*/
	Scale10(  100,   4467  ),  /*C/N=10.0dB*/
	Scale10(  105,   4236  ),  /*C/N=10.5dB*/
	Scale10(  110,   4013  ),  /*C/N=11.0dB*/
	Scale10(  115,   3800  ),  /*C/N=11.5dB*/
	Scale10(  120,   3598  ),  /*C/N=12.0dB*/
	Scale10(  125,   3406  ),  /*C/N=12.5dB*/
	Scale10(  130,   3225  ),  /*C/N=13.0dB*/
	Scale10(  135,   3052  ),  /*C/N=13.5dB*/
	Scale10(  140,   2889  ),  /*C/N=14.0dB*/
	Scale10(  145,   2733  ),  /*C/N=14.5dB*/
	Scale10(  150,   2587  ),  /*C/N=15.0dB*/
	Scale10(  160,   2318  ),  /*C/N=16.0dB*/
	Scale10(  170,   2077  ),  /*C/N=17.0dB*/
	Scale10(  180,   1862  ),  /*C/N=18.0dB*/
	Scale10(  190,   1670  ),  /*C/N=19.0dB*/
	Scale10(  200,   1499  ),  /*C/N=20.0dB*/
	Scale10(  210,   1347  ),  /*C/N=21.0dB*/
	Scale10(  220,   1213  ),  /*C/N=22.0dB*/
	Scale10(  230,   1095  ),  /*C/N=23.0dB*/
	Scale10(  240,    992  ),  /*C/N=24.0dB*/
	Scale10(  250,    900  ),  /*C/N=25.0dB*/
	Scale10(  260,    826  ),  /*C/N=26.0dB*/
	Scale10(  270,    758  ),  /*C/N=27.0dB*/
	Scale10(  280,    702  ),  /*C/N=28.0dB*/
	Scale10(  290,    653  ),  /*C/N=29.0dB*/
	Scale10(  300,    613  ),  /*C/N=30.0dB*/
	Scale10(  310,    579  ),  /*C/N=31.0dB*/
	Scale10(  320,    550  ),  /*C/N=32.0dB*/
	Scale10(  330,    526  ),  /*C/N=33.0dB*/
	Scale10(  350,    490  ),  /*C/N=35.0dB*/
	Scale10(  400,    445  ),  /*C/N=40.0dB*/
	Scale10(  450,    430  ),  /*C/N=45.0dB*/
	Scale10(  500,    426  ),  /*C/N=50.0dB*/
	Scale10(  510,    425  ),  /*C/N=51.0dB*/
	Scale10(  515,     -1  )
};

static const struct LookupTable s2_cnr_lookup[] = {
	Scale10(  -35,  65536  ),
	Scale10(  -30,  13950  ),  /*C/N=-3.0dB*/
	Scale10(  -25,  13580  ),  /*C/N=-2.5dB*/
	Scale10(  -20,  13150  ),  /*C/N=-2.0dB*/
	Scale10(  -15,  12760  ),  /*C/N=-1.5dB*/
	Scale10(  -10,  12345  ),  /*C/N=-1.0dB*/
	Scale10(   -5,  11900  ),  /*C/N=-0.5dB*/
	Scale10(    0,  11520  ),  /*C/N=   0dB*/
	Scale10(    5,  11080  ),  /*C/N= 0.5dB*/
	Scale10(   10,  10630  ),  /*C/N= 1.0dB*/
	Scale10(   15,  10210  ),  /*C/N= 1.5dB*/
	Scale10(   20,   9790  ),  /*C/N= 2.0dB*/
	Scale10(   25,   9390  ),  /*C/N= 2.5dB*/
	Scale10(   30,   8970  ),  /*C/N= 3.0dB*/
	Scale10(   35,   8575  ),  /*C/N= 3.5dB*/
	Scale10(   40,   8180  ),  /*C/N= 4.0dB*/
	Scale10(   45,   7800  ),  /*C/N= 4.5dB*/
	Scale10(   50,   7430  ),  /*C/N= 5.0dB*/
	Scale10(   55,   7080  ),  /*C/N= 5.5dB*/
	Scale10(   60,   6720  ),  /*C/N= 6.0dB*/
	Scale10(   65,   6320  ),  /*C/N= 6.5dB*/
	Scale10(   70,   6060  ),  /*C/N= 7.0dB*/
	Scale10(   75,   5760  ),  /*C/N= 7.5dB*/
	Scale10(   80,   5480  ),  /*C/N= 8.0dB*/
	Scale10(   85,   5200  ),  /*C/N= 8.5dB*/
	Scale10(   90,   4930  ),  /*C/N= 9.0dB*/
	Scale10(   95,   4680  ),  /*C/N= 9.5dB*/
	Scale10(  100,   4425  ),  /*C/N=10.0dB*/
	Scale10(  105,   4210  ),  /*C/N=10.5dB*/
	Scale10(  110,   3980  ),  /*C/N=11.0dB*/
	Scale10(  115,   3765  ),  /*C/N=11.5dB*/
	Scale10(  120,   3570  ),  /*C/N=12.0dB*/
	Scale10(  125,   3315  ),  /*C/N=12.5dB*/
	Scale10(  130,   3140  ),  /*C/N=13.0dB*/
	Scale10(  135,   2980  ),  /*C/N=13.5dB*/
	Scale10(  140,   2820  ),  /*C/N=14.0dB*/
	Scale10(  145,   2670  ),  /*C/N=14.5dB*/
	Scale10(  150,   2535  ),  /*C/N=15.0dB*/
	Scale10(  160,   2270  ),  /*C/N=16.0dB*/
	Scale10(  170,   2035  ),  /*C/N=17.0dB*/
	Scale10(  180,   1825  ),  /*C/N=18.0dB*/
	Scale10(  190,   1650  ),  /*C/N=19.0dB*/
	Scale10(  200,   1485  ),  /*C/N=20.0dB*/
	Scale10(  210,   1340  ),  /*C/N=21.0dB*/
	Scale10(  220,   1212  ),  /*C/N=22.0dB*/
	Scale10(  230,   1100  ),  /*C/N=23.0dB*/
	Scale10(  240,   1000  ),  /*C/N=24.0dB*/
	Scale10(  250,    910  ),  /*C/N=25.0dB*/
	Scale10(  260,    836  ),  /*C/N=26.0dB*/
	Scale10(  270,    772  ),  /*C/N=27.0dB*/
	Scale10(  280,    718  ),  /*C/N=28.0dB*/
	Scale10(  290,    671  ),  /*C/N=29.0dB*/
	Scale10(  300,    635  ),  /*C/N=30.0dB*/
	Scale10(  310,    602  ),  /*C/N=31.0dB*/
	Scale10(  320,    575  ),  /*C/N=32.0dB*/
	Scale10(  330,    550  ),  /*C/N=33.0dB*/
	Scale10(  350,    517  ),  /*C/N=35.0dB*/
	Scale10(  400,    480  ),  /*C/N=40.0dB*/
	Scale10(  450,    466  ),  /*C/N=45.0dB*/
	Scale10(  500,    464  ),  /*C/N=50.0dB*/
	Scale10(  510,    463  ),  /*C/N=51.0dB*/
	Scale10(  515,     -1  )
};

static const struct LookupTable agc_gain_table_6120[] = {
	Scale100(   779,   65536 ),
	Scale100(   880,   65535 ),
	Scale100(   898,   65534 ),
	Scale100(   930,   50816 ),
	Scale100(   976,   48164 ),
	Scale100(  1080,   46720 ),
	Scale100(  1188,   45712 ),
	Scale100(  1291,   44913 ),
	Scale100(  1412,   44288 ),
	Scale100(  1537,   43743 ),
	Scale100(  1637,   43263 ),
	Scale100(  1764,   42815 ),
	Scale100(  1875,   42354 ),
	Scale100(  1999,   41934 ),
	Scale100(  2088,   41631 ),
	Scale100(  2189,   41295 ),
	Scale100(  2290,   40976 ),
	Scale100(  2392,   40640 ),
	Scale100(  2497,   40351 ),
	Scale100(  2592,   40064 ),
	Scale100(  2692,   39792 ),
	Scale100(  2792,   39520 ),
	Scale100(  2900,   39226 ),
	Scale100(  2993,   38976 ),
	Scale100(  3091,   38720 ),
	Scale100(  3191,   38479 ),
	Scale100(  3281,   38240 ),
	Scale100(  3358,   38048 ),
	Scale100(  3458,   37823 ),
	Scale100(  3544,   37600 ),
	Scale100(  3648,   37375 ),
	Scale100(  3738,   37152 ),
	Scale100(  3838,   36912 ),
	Scale100(  3930,   36704 ),
	Scale100(  4020,   36500 ),
	Scale100(  4113,   36289 ),
	Scale100(  4279,   36095 ),
	Scale100(  4365,   35870 ),
	Scale100(  4426,   35680 ),
	Scale100(  4500,   35488 ),
	Scale100(  4582,   35295 ),
	Scale100(  4666,   35103 ),
	Scale100(  4740,   34910 ),
	Scale100(  4820,   34719 ),
	Scale100(  4884,   34543 ),
	Scale100(  4963,   34352 ),
	Scale100(  5048,   34160 ),
	Scale100(  5125,   33967 ),
	Scale100(  5198,   33775 ),
	Scale100(  5271,   33584 ),
	Scale100(  5340,   33392 ),
	Scale100(  5414,   33184 ),
	Scale100(  5498,   32960 ),
	Scale100(  5582,   32752 ),
	Scale100(  5659,   32544 ),
	Scale100(  5730,   32320 ),
	Scale100(  5801,   32096 ),
	Scale100(  5883,   31840 ),
	Scale100(  5956,   31600 ),
	Scale100(  6028,   31345 ),
	Scale100(  6101,   31088 ),
	Scale100(  6179,   30815 ),
	Scale100(  6263,   30490 ),
	Scale100(  6341,   30180 ),
	Scale100(  6416,   29857 ),
	Scale100(  6494,   29504 ),
	Scale100(  6571,   29120 ),
	Scale100(  6645,   28737 ),
	Scale100(  6728,   28271 ),
	Scale100(  6809,   27741 ),
	Scale100(  6897,   27100 ),
	Scale100(  6988,   26305 ),
	Scale100(  7090,   25123 ),
	Scale100(  7214,   23432 ),
	Scale100(  7368,   18711 ),
	Scale100(  7429,       0 ),
	Scale100(  7430,      -1 )
};

static const struct LookupTable adc_power_table[] = {
    Scale100(     0,16777216 ),
    Scale100(     0,  118000 ),
    Scale100(  -100,   93600 ),
    Scale100(  -200,   74500 ),
    Scale100(  -300,   59100 ),
    Scale100(  -400,   47000 ),
    Scale100(  -500,   37300 ),
    Scale100(  -600,   29650 ),
    Scale100(  -700,   23520 ),
    Scale100(  -900,   14850 ),
    Scale100( -1100,    9380 ),
    Scale100( -1300,    5910 ),
    Scale100( -1500,    3730 ),
    Scale100( -1700,    2354 ),
    Scale100( -1900,    1485 ),
    Scale100( -2000,    1179 ),
    Scale100( -2100,    1178 ),
    Scale100( -2100,      -1 )
};

static const u8 s2CarLoop[] =	{
/*********************************************************************
 Tracking carrier loop carrier QPSK 1/4 to 8PSK 9/10 long Frame
 *********************************************************************/
/* Modcod  2MPon 2MPoff 5MPon 5MPoff 10MPon 10MPoff 20MPon 20MPoff 30MPon 30MPoff*/
	/* FE_QPSK_14  */
	0x0C,  0x3C,  0x0B,  0x3C,  0x2A,  0x2C,  0x2A,  0x1C,  0x3A,  0x3B,
	/* FE_QPSK_13  */
	0x0C,  0x3C,  0x0B,  0x3C,  0x2A,  0x2C,  0x3A,  0x0C,  0x3A,  0x2B,
	/* FE_QPSK_25  */
	0x1C,  0x3C,  0x1B,  0x3C,  0x3A,  0x1C,  0x3A,  0x3B,  0x3A,  0x2B,
	/* FE_QPSK_12  */
	0x0C,  0x1C,  0x2B,  0x1C,  0x0B,  0x2C,  0x0B,  0x0C,  0x2A,  0x2B,
	/* FE_QPSK_35  */
	0x1C,  0x1C,  0x2B,  0x1C,  0x0B,  0x2C,  0x0B,  0x0C,  0x2A,  0x2B,
	/* FE_QPSK_23  */
	0x2C,  0x2C,  0x2B,  0x1C,  0x0B,  0x2C,  0x0B,  0x0C,  0x2A,  0x2B,
	/* FE_QPSK_34  */
	0x3C,  0x2C,  0x3B,  0x2C,  0x1B,  0x1C,  0x1B,  0x3B,  0x3A,  0x1B,
	/* FE_QPSK_45  */
	0x0D,  0x3C,  0x3B,  0x2C,  0x1B,  0x1C,  0x1B,  0x3B,  0x3A,  0x1B,
	/* FE_QPSK_56  */
	0x1D,  0x3C,  0x0C,  0x2C,  0x2B,  0x1C,  0x1B,  0x3B,  0x0B,  0x1B,
	/* FE_QPSK_89  */
	0x3D,  0x0D,  0x0C,  0x2C,  0x2B,  0x0C,  0x2B,  0x2B,  0x0B,  0x0B,
	/* FE_QPSK_910 */
	0x1E,  0x0D,  0x1C,  0x2C,  0x3B,  0x0C,  0x2B,  0x2B,  0x1B,  0x0B,
	/* FE_8PSK_35  */
	0x28,  0x09,  0x28,  0x09,  0x28,  0x09,  0x28,  0x08,  0x28,  0x27,
	/* FE_8PSK_23  */
	0x19,  0x29,  0x19,  0x29,  0x19,  0x29,  0x38,  0x19,  0x28,  0x09,
	/* FE_8PSK_34  */
	0x1A,  0x0B,  0x1A,  0x3A,  0x0A,  0x2A,  0x39,  0x2A,  0x39,  0x1A,
	/* FE_8PSK_56  */
	0x2B,  0x2B,  0x1B,  0x1B,  0x0B,  0x1B,  0x1A,  0x0B,  0x1A,  0x1A,
	/* FE_8PSK_89  */
	0x0C,  0x0C,  0x3B,  0x3B,  0x1B,  0x1B,  0x2A,  0x0B,  0x2A,  0x2A,
	/* FE_8PSK_910 */
	0x0C,  0x1C,  0x0C,  0x3B,  0x2B,  0x1B,  0x3A,  0x0B,  0x2A,  0x2A,

/**********************************************************************
 Tracking carrier loop carrier 16APSK 2/3 to 32APSK 9/10 long Frame
 **********************************************************************/
/*Modcod 2MPon  2MPoff 5MPon 5MPoff 10MPon 10MPoff 20MPon 20MPoff 30MPon 30MPoff*/
	/* FE_16APSK_23  */
	0x0A,  0x0A,  0x0A,  0x0A,  0x1A,  0x0A,  0x39,  0x0A,  0x29,  0x0A,
	/* FE_16APSK_34  */
	0x0A,  0x0A,  0x0A,  0x0A,  0x0B,  0x0A,  0x2A,  0x0A,  0x1A,  0x0A,
	/* FE_16APSK_45  */
	0x0A,  0x0A,  0x0A,  0x0A,  0x1B,  0x0A,  0x3A,  0x0A,  0x2A,  0x0A,
	/* FE_16APSK_56  */
	0x0A,  0x0A,  0x0A,  0x0A,  0x1B,  0x0A,  0x3A,  0x0A,  0x2A,  0x0A,
	/* FE_16APSK_89  */
	0x0A,  0x0A,  0x0A,  0x0A,  0x2B,  0x0A,  0x0B,  0x0A,  0x3A,  0x0A,
	/* FE_16APSK_910 */
	0x0A,  0x0A,  0x0A,  0x0A,  0x2B,  0x0A,  0x0B,  0x0A,  0x3A,  0x0A,
	/* FE_32APSK_34  */
	0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,
	/* FE_32APSK_45  */
	0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,
	/* FE_32APSK_56  */
	0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,
	/* FE_32APSK_89  */
	0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,
	/* FE_32APSK_910 */
	0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,  0x09,
};

static const u8 error_source_select[] = {
	STV0910_ERR_SOURCE_PER,
	STV0910_ERR_SOURCE_BER,
	STV0910_ERR_SOURCE_OUTER,
	STV0910_ERR_SOURCE_INNER,
};

enum ErrorRateUnit {
	BaseByte,
	BasePacket,
	BaseFrame,
};

static const enum ErrorRateUnit error_unit[][2] = {
	{	BasePacket,	BasePacket	},
	{	BaseByte,	BaseByte	},
	{	BaseByte,	BaseFrame	},
	{	BaseFrame,	BaseFrame	},
};

static const struct stv0910_init_pair initPairs[] = {
	{ RSTV0910_DACR1,                   0x00 },    //    2  f113
	{ RSTV0910_DACR2,                   0x00 },    //    3  f114
	{ RSTV0910_PADCFG,                  0x05 },    //    4  f11a
	{ RSTV0910_OUTCFG2,                 0x00 },    //    5  f11b
	{ RSTV0910_OUTCFG,                  0x00 },    //    6  f11c
	{ RSTV0910_IRQMASK3,                0x00 },    //   11  f124
	{ RSTV0910_IRQMASK2,                0x00 },    //   12  f125
	{ RSTV0910_IRQMASK1,                0x00 },    //   13  f126
	{ RSTV0910_IRQMASK0,                0x00 },    //   14  f127
	{ RSTV0910_I2CCFG,                  0x88 },    //   15  f129
	{ RSTV0910_GPIO0CFG,                0x82 },    //   18  f140
	{ RSTV0910_GPIO1CFG,                0x82 },    //   19  f141
	{ RSTV0910_GPIO2CFG,                0x82 },    //   20  f142
	{ RSTV0910_GPIO3CFG,                0x82 },    //   21  f143
	{ RSTV0910_GPIO4CFG,                0x82 },    //   22  f144
	{ RSTV0910_GPIO5CFG,                0x82 },    //   23  f145
	{ RSTV0910_GPIO6CFG,                0x82 },    //   24  f146
	{ RSTV0910_GPIO7CFG,                0x82 },    //   25  f147
	{ RSTV0910_GPIO8CFG,                0x82 },    //   26  f148
	{ RSTV0910_GPIO9CFG,                0x82 },    //   27  f149
	{ RSTV0910_GPIO10CFG,               0x82 },    //   28  f14a
	{ RSTV0910_GPIO11CFG,               0x82 },    //   29  f14b
	{ RSTV0910_GPIO12CFG,               0x82 },    //   30  f14c
	{ RSTV0910_GPIO13CFG,               0x82 },    //   31  f14d
	{ RSTV0910_GPIO14CFG,               0x82 },    //   32  f14e
	{ RSTV0910_GPIO15CFG,               0x82 },    //   33  f14f
	{ RSTV0910_GPIO16CFG,               0x82 },    //   34  f150
	{ RSTV0910_GPIO17CFG,               0x82 },    //   35  f151
	{ RSTV0910_GPIO18CFG,               0x82 },    //   36  f152
	{ RSTV0910_GPIO19CFG,               0x82 },    //   37  f153
	{ RSTV0910_GPIO20CFG,               0x82 },    //   38  f154
	{ RSTV0910_GPIO21CFG,               0x82 },    //   39  f155
	{ RSTV0910_GPIO22CFG,               0x82 },    //   40  f156
	{ RSTV0910_NCOARSE,                 0x39 },    //   69  f1b3
	{ RSTV0910_NCOARSE1,                0x12 },    //   70  f1b4
	{ RSTV0910_NCOARSE2,                0x04 },    //   71  f1b5
	{ RSTV0910_SYNTCTRL,                0x02 },    //   72  f1b6
	{ RSTV0910_FILTCTRL,                0x01 },    //   73  f1b7
	{ RSTV0910_STOPCLK1,                0x00 },    //   75  f1c2
	{ RSTV0910_STOPCLK2,                0x00 },    //   76  f1c3
	{ RSTV0910_PREGCTL,                 0x00 },    //   77  f1c8
	{ RSTV0910_TSTTNR0,                 0x04 },    //   78  f1df
	{ RSTV0910_TSTTNR1,                 0x26 },    //   79  f1e0
	{ RSTV0910_TSTTNR2,                 0x6b },    //   80  f1e1
	{ RSTV0910_TSTTNR3,                 0x26 },    //   81  f1e2
	{ RSTV0910_TSGENERAL,               0x00 },    //  738  f630
	{ 0xfa34,                           0x05 },    //  839  fa34
	{ 0xfa35,                           0x5b },    //  840  fa35
	{ 0xfa36,                           0x96 },    //  841  fa36
	{ 0xfa37,                           0x00 },    //  842  fa37
	{ 0xfa38,                           0x00 },    //  843  fa38
	{ 0xfa39,                           0x00 },    //  844  fa39
	{ 0xfa3a,                           0x00 },    //  845  fa3a
	{ 0xfa40,                           0x20 },    //  846  fa40
	{ 0xfa41,                           0x20 },    //  847  fa41
	{ 0xfa42,                           0x20 },    //  848  fa42
	{ RSTV0910_GAINLLR_NF4,             0x20 },    //  849  fa43
	{ RSTV0910_GAINLLR_NF5,             0x20 },    //  850  fa44
	{ RSTV0910_GAINLLR_NF6,             0x20 },    //  851  fa45
	{ RSTV0910_GAINLLR_NF7,             0x20 },    //  852  fa46
	{ RSTV0910_GAINLLR_NF8,             0x20 },    //  853  fa47
	{ RSTV0910_GAINLLR_NF9,             0x20 },    //  854  fa48
	{ RSTV0910_GAINLLR_NF10,            0x20 },    //  855  fa49
	{ RSTV0910_GAINLLR_NF11,            0x20 },    //  856  fa4a
	{ RSTV0910_GAINLLR_NF12,            0x20 },    //  857  fa4b
	{ RSTV0910_GAINLLR_NF13,            0x20 },    //  858  fa4c
	{ RSTV0910_GAINLLR_NF14,            0x20 },    //  859  fa4d
	{ RSTV0910_GAINLLR_NF15,            0x20 },    //  860  fa4e
	{ RSTV0910_GAINLLR_NF16,            0x20 },    //  861  fa4f
	{ RSTV0910_GAINLLR_NF17,            0x20 },    //  862  fa50
	{ 0xfa51,                           0x22 },    //  863  fa51
	{ 0xfa52,                           0x22 },    //  864  fa52
	{ 0xfa53,                           0x24 },    //  865  fa53
	{ 0xfa54,                           0x24 },    //  866  fa54
	{ 0xfa55,                           0x25 },    //  867  fa55
	{ 0xfa56,                           0x26 },    //  868  fa56
	{ 0xfa57,                           0x20 },    //  869  fa57
	{ 0xfa58,                           0x20 },    //  870  fa58
	{ 0xfa59,                           0x20 },    //  871  fa59
	{ 0xfa5a,                           0x20 },    //  872  fa5a
	{ 0xfa5b,                           0x20 },    //  873  fa5b
	{ 0xfa5c,                           0x20 },    //  874  fa5c
	{ 0xfa5d,                           0x20 },    //  875  fa5d
	{ 0xfa5e,                           0x20 },    //  876  fa5e
	{ 0xfa5f,                           0x20 },    //  877  fa5f
	{ 0xfa60,                           0x20 },    //  878  fa60
	{ 0xfa61,                           0x20 },    //  879  fa61
	{ 0xfa62,                           0x20 },    //  880  fa62
	{ 0xfa63,                           0x20 },    //  881  fa63
	{ 0xfa64,                           0x20 },    //  882  fa64
	{ 0xfa65,                           0x20 },    //  883  fa65
	{ 0xfa66,                           0x20 },    //  884  fa66
	{ 0xfa67,                           0x20 },    //  885  fa67
	{ 0xfa68,                           0x20 },    //  886  fa68
	{ 0xfa69,                           0x20 },    //  887  fa69
	{ 0xfa6a,                           0x20 },    //  888  fa6a
	{ 0xfa6b,                           0x22 },    //  889  fa6b
	{ 0xfa6c,                           0x22 },    //  890  fa6c
	{ 0xfa6d,                           0x24 },    //  891  fa6d
	{ 0xfa6e,                           0x24 },    //  892  fa6e
	{ 0xfa6f,                           0x25 },    //  893  fa6f
	{ 0xfa70,                           0x20 },    //  894  fa70
	{ 0xfa71,                           0x20 },    //  895  fa71
	{ 0xfa72,                           0x20 },    //  896  fa72
	{ 0xfa73,                           0x20 },    //  897  fa73
	{ RSTV0910_CFGEXT,                  0x02 },    //  898  fa80
	{ RSTV0910_GENCFG,                  0x15 },    //  899  fa86
	{ RSTV0910_TSTRES0,                 0x00 },    //  961  ff11
	{ 0xff12,                           0x00 },    //  962  ff12
	{ 0xff13,                           0x00 },    //  963  ff13
	{ 0xff6d,                           0x00 },    //  972  ff6d
};

static const struct stv0910_init_pair initPairs0[] = {
	{ RSTV0910_P1_I2CRPT,               0x24 },    //   16  f12a
	{ RSTV0910_P1_IQCONST,              0x00 },    //  406  f400
	{ RSTV0910_P1_NOSCFG,               0x14 },    //  407  f401
	{ RSTV0910_P1_AGC1CFG,              0x54 },    //  410  f404
	{ RSTV0910_P1_AGC1CN,               0x99 },    //  411  f406
	{ RSTV0910_P1_AGC1REF,              0x58 },    //  412  f407
	{ RSTV0910_P1_DEMOD,                0x00 },    //  421  f410
	{ RSTV0910_P1_DMDMODCOD,            0x10 },    //  422  f411
	{ RSTV0910_P1_DMDCFGMD,             0xc9 },    //  425  f414
	{ RSTV0910_P1_DMDCFG2,              0x3b },    //  426  f415
	{ RSTV0910_P1_DMDISTATE,            0x5c },    //  427  f416
	{ RSTV0910_P1_DMDT0M,               0x40 },    //  428  f417
	{ RSTV0910_P1_DMDCFG3,              0x08 },    //  432  f41e
	{ RSTV0910_P1_DMDCFG4,              0x04 },    //  433  f41f
	{ RSTV0910_P1_CORRELMANT,           0x78 },    //  434  f420
	{ RSTV0910_P1_CORRELABS,            0x80 },    //  435  f421
	{ RSTV0910_P1_CORRELEXP,            0xaa },    //  436  f422
	{ RSTV0910_P1_DMDREG,               0x01 },    //  438  f425
	{ 0xf426,                           0x00 },    //  439  f426
	{ 0xf427,                           0x00 },    //  440  f427
	{ 0xf428,                           0x00 },    //  441  f428
	{ 0xf429,                           0x00 },    //  442  f429
	{ 0xf42a,                           0x00 },    //  443  f42a
	{ 0xf42b,                           0x00 },    //  444  f42b
	{ RSTV0910_P1_AGC2O,                0x5b },    //  445  f42c
	{ RSTV0910_P1_AGC2REF,              0x38 },    //  446  f42d
	{ RSTV0910_P1_AGC1ADJ,              0x58 },    //  447  f42e
	{ 0xf42f,                           0x38 },    //  448  f42f
	{ 0xf430,                           0x38 },    //  449  f430
	{ 0xf431,                           0x38 },    //  450  f431
	{ 0xf432,                           0x38 },    //  451  f432
	{ 0xf433,                           0x38 },    //  452  f433
	{ 0xf434,                           0x47 },    //  453  f434
	{ 0xf435,                           0x38 },    //  454  f435
	{ RSTV0910_P1_AGC2I1,               0x0b },    //  455  f436
	{ RSTV0910_P1_AGC2I0,               0x61 },    //  456  f437
	{ RSTV0910_P1_CARCFG,               0x46 },    //  457  f438
	{ RSTV0910_P1_ACLC,                 0x2b },    //  458  f439
	{ RSTV0910_P1_BCLC,                 0x1a },    //  459  f43a
	{ 0xf43b,                           0x00 },    //  460  f43b
	{ 0xf43c,                           0x00 },    //  461  f43c
	{ RSTV0910_P1_CARFREQ,              0x79 },    //  462  f43d
	{ RSTV0910_P1_CARHDR,               0x1c },    //  463  f43e
	{ RSTV0910_P1_LDT,                  0xd0 },    //  464  f43f
	{ RSTV0910_P1_LDT2,                 0xb8 },    //  465  f440
	{ RSTV0910_P1_CFRICFG,              0xf8 },    //  466  f441
	{ RSTV0910_P1_CFRUP1,               0x0e },    //  467  f442
	{ RSTV0910_P1_CFRUP0,               0x69 },    //  468  f443
	{ RSTV0910_P1_CFRIBASE1,            0xff },    //  469  f444
	{ RSTV0910_P1_CFRIBASE0,            0x7a },    //  470  f445
	{ RSTV0910_P1_CFRLOW1,              0xf1 },    //  471  f446
	{ RSTV0910_P1_CFRLOW0,              0x97 },    //  472  f447
	{ RSTV0910_P1_CFRINIT1,             0xff },    //  473  f448
	{ RSTV0910_P1_CFRINIT0,             0x7a },    //  474  f449
	{ RSTV0910_P1_CFRINC1,              0x03 },    //  475  f44a
	{ RSTV0910_P1_CFRINC0,              0x8e },    //  476  f44b
	{ RSTV0910_P1_CFR2,                 0xff },    //  477  f44c
	{ RSTV0910_P1_CFR1,                 0x7a },    //  478  f44d
	{ RSTV0910_P1_CFR0,                 0x00 },    //  479  f44e
	{ RSTV0910_P1_TMGCFG,               0xd3 },    //  481  f450
	{ RSTV0910_P1_RTC,                  0x68 },    //  482  f451
	{ RSTV0910_P1_RTCS2,                0x55 },    //  483  f452
	{ RSTV0910_P1_TMGTHRISE,            0x08 },    //  484  f453
	{ RSTV0910_P1_TMGTHFALL,            0x08 },    //  485  f454
	{ RSTV0910_P1_SFRUPRATIO,           0x20 },    //  486  f455
	{ RSTV0910_P1_SFRLOWRATIO,          0xd0 },    //  487  f456
	{ RSTV0910_P1_KTTMG,                0xa0 },    //  488  f457
	{ RSTV0910_P1_KREFTMG,              0x80 },    //  489  f458
	{ RSTV0910_P1_SFRSTEP,              0x88 },    //  490  f459
	{ RSTV0910_P1_TMGCFG2,              0x80 },    //  491  f45a
	{ RSTV0910_P1_KREFTMG2,             0x80 },    //  492  f45b
	{ RSTV0910_P1_TMGCFG3,              0x06 },    //  493  f45d
	{ RSTV0910_P1_SFRINIT1,             0x38 },    //  494  f45e
	{ RSTV0910_P1_SFRINIT0,             0xe3 },    //  495  f45f
	{ RSTV0910_P1_SFRUP1,               0x19 },    //  496  f460
	{ RSTV0910_P1_SFRUP0,               0x99 },    //  497  f461
	{ RSTV0910_P1_SFRLOW1,              0x12 },    //  498  f462
	{ RSTV0910_P1_SFRLOW0,              0x7d },    //  499  f463
	{ RSTV0910_P1_SFR3,                 0x38 },    //  500  f464
	{ RSTV0910_P1_SFR2,                 0xe3 },    //  501  f465
	{ RSTV0910_P1_SFR1,                 0x00 },    //  502  f466
	{ RSTV0910_P1_SFR0,                 0x00 },    //  503  f467
	{ RSTV0910_P1_TMGREG2,              0x00 },    //  504  f468
	{ RSTV0910_P1_TMGREG1,              0x00 },    //  505  f469
	{ RSTV0910_P1_TMGREG0,              0x00 },    //  506  f46a
	{ RSTV0910_P1_EQUALCFG,             0x41 },    //  510  f46f
	{ RSTV0910_P1_NOSCFGF1,             0x00 },    //  541  f48e
	{ 0xf48f,                           0x00 },    //  542  f48f
	{ RSTV0910_P1_CAR2CFG,              0x06 },    //  543  f490
	{ RSTV0910_P1_CFR2CFR1,             0xe5 },    //  544  f491
	{ RSTV0910_P1_CAR3CFG,              0x02 },    //  545  f492
	{ RSTV0910_P1_CFR22,                0x00 },    //  546  f493
	{ RSTV0910_P1_CFR21,                0x00 },    //  547  f494
	{ RSTV0910_P1_CFR20,                0x00 },    //  548  f495
	{ RSTV0910_P1_ACLC2S2Q,             0x0b },    //  549  f497
	{ RSTV0910_P1_ACLC2S28,             0x0a },    //  550  f498
	{ RSTV0910_P1_ACLC2S216A,           0x49 },    //  551  f499
	{ RSTV0910_P1_ACLC2S232A,           0x48 },    //  552  f49a
	{ RSTV0910_P1_BCLC2S2Q,             0x84 },    //  553  f49c
	{ RSTV0910_P1_BCLC2S28,             0x84 },    //  554  f49d
	{ 0xf49e,                           0x84 },    //  555  f49e
	{ 0xf49f,                           0x84 },    //  556  f49f
	{ RSTV0910_P1_PLROOT2,              0x00 },    //  557  f4ac
	{ RSTV0910_P1_PLROOT1,              0x00 },    //  558  f4ad
	{ RSTV0910_P1_PLROOT0,              0x01 },    //  559  f4ae
	{ RSTV0910_P1_MODCODLST0,			0xff },    //  560  f4b0
	{ RSTV0910_P1_MODCODLST1,			0xfc },    //  561  f4b1
	{ RSTV0910_P1_MODCODLST2,			0x00 },    //  562  f4b2
	{ RSTV0910_P1_MODCODLST3,			0x00 },    //  563  f4b3
	{ RSTV0910_P1_MODCODLST4,			0xc0 },    //  564  f4b4
	{ RSTV0910_P1_MODCODLST5,			0x00 },    //  565  f4b5
	{ RSTV0910_P1_MODCODLST6,			0x00 },    //  566  f4b6
	{ RSTV0910_P1_MODCODLST7,           0xc0 },    //  567  f4b7
	{ RSTV0910_P1_MODCODLST8,           0x00 },    //  568  f4b8
	{ RSTV0910_P1_MODCODLST9,           0x00 },    //  569  f4b9
	{ RSTV0910_P1_MODCODLSTA,           0xc0 },    //  570  f4ba
	{ RSTV0910_P1_MODCODLSTB,           0x00 },    //  571  f4bb
	{ RSTV0910_P1_MODCODLSTC,           0x00 },    //  572  f4bc
	{ RSTV0910_P1_MODCODLSTD,           0x00 },    //  573  f4bd
	{ RSTV0910_P1_MODCODLSTE,			0x00 },    //  574  f4be
	{ RSTV0910_P1_MODCODLSTF,			0x0f },    //  575  f4bf
	{ RSTV0910_P1_GAUSSR0,              0x98 },    //  576  f4c0
	{ RSTV0910_P1_CCIR0,                0x30 },    //  577  f4c1
	{ RSTV0910_P1_CCIQUANT,             0xac },    //  578  f4c2
	{ RSTV0910_P1_CCITHRES,             0x50 },    //  579  f4c3
	{ RSTV0910_P1_CCIACC,               0x00 },    //  580  f4c4
	{ RSTV0910_P1_DMDRESCFG,            0x29 },    //  582  f4c6
	{ RSTV0910_P1_FFECFG,               0x71 },    //  600  f4d8
	{ RSTV0910_P1_TNRCFG2,              0x02 },    //  601  f4e1
	{ RSTV0910_P1_SMAPCOEF7,            0x00 },    //  602  f500
	{ RSTV0910_P1_SMAPCOEF6,            0x00 },    //  603  f501
	{ RSTV0910_P1_SMAPCOEF5,            0x00 },    //  604  f502
	{ 0xf503,                           0x00 },    //  605  f503
	{ 0xf504,                           0x00 },    //  606  f504
	{ 0xf505,                           0x00 },    //  607  f505
	{ 0xf506,                           0x00 },    //  608  f506
	{ 0xf507,                           0x71 },    //  609  f507
	{ RSTV0910_P1_NOSTHRES1,            0x60 },    //  610  f509
	{ RSTV0910_P1_NOSTHRES2,            0x69 },    //  611  f50a
	{ RSTV0910_P1_NOSDIFF1,             0x80 },    //  612  f50b
	{ RSTV0910_P1_RAINFADE,             0x35 },    //  613  f50c
	{ RSTV0910_P1_NOSRAMCFG,            0x28 },    //  614  f50d
	{ RSTV0910_P1_NOSRAMPOS,            0x26 },    //  615  f50e
	{ RSTV0910_P1_NOSRAMVAL,            0x86 },    //  616  f50f
	{ RSTV0910_P1_VITSCALE,             0x80 },    //  622  f532
	{ RSTV0910_P1_FECM,                 0x00 },    //  623  f533
	{ RSTV0910_P1_VTH12,                0xd7 },    //  624  f534
	{ RSTV0910_P1_VTH23,                0x85 },    //  625  f535
	{ RSTV0910_P1_VTH34,                0x58 },    //  626  f536
	{ RSTV0910_P1_VTH56,                0x3a },    //  627  f537
	{ RSTV0910_P1_VTH67,                0x34 },    //  628  f538
	{ RSTV0910_P1_VTH78,                0x28 },    //  629  f539
	{ RSTV0910_P1_PRVIT,                0x2f },    //  632  f53c
	{ RSTV0910_P1_VAVSRVIT,             0x00 },    //  633  f53d
	{ RSTV0910_P1_KDIV12,               0x27 },    //  636  f540
	{ RSTV0910_P1_KDIV23,               0x32 },    //  637  f541
	{ RSTV0910_P1_KDIV34,               0x32 },    //  638  f542
	{ RSTV0910_P1_KDIV56,               0x32 },    //  639  f543
	{ RSTV0910_P1_KDIV67,               0x32 },    //  640  f544
	{ RSTV0910_P1_KDIV78,               0x50 },    //  641  f545
	{ 0xf546,                           0x00 },    //  642  f546
	{ 0xf547,                           0x00 },    //  643  f547
	{ RSTV0910_P1_PDELCTRL0,            0x01 },    //  644  f54f
	{ RSTV0910_P1_PDELCTRL1,            0x00 },    //  645  f550
	{ RSTV0910_P1_PDELCTRL2,            0x00 },    //  646  f551
	{ RSTV0910_P1_HYSTTHRESH,           0x41 },    //  647  f554
	{ RSTV0910_P1_ISIENTRY,             0x00 },    //  648  f55e
	{ RSTV0910_P1_ISIBITENA,            0x00 },    //  649  f55f
	{ RSTV0910_P1_PDELCTRL3,            0x00 },    //  665  f56f
	{ RSTV0910_P1_TSSTATEM,             0xf0 },    //  666  f570
	{ 0xf571,                           0x12 },    //  667  f571
	{ RSTV0910_P1_TSCFGH,               0x00 },    //  668  f572
	{ RSTV0910_P1_TSCFGM,               0x04 },    //  669  f573
	{ RSTV0910_P1_TSCFGL,               0x20 },    //  670  f574
	{ 0xf575,                           0x00 },    //  671  f575
	{ RSTV0910_P1_TSINSDELH,            0x00 },    //  672  f576
	{ 0xf577,                           0x00 },    //  673  f577
	{ 0xf578,                           0x00 },    //  674  f578
	{ RSTV0910_P1_TSDIVN,               0x03 },    //  675  f579
	{ RSTV0910_P1_TSCFG4,               0x00 },    //  676  f57a
	{ RSTV0910_P1_TSSPEED,              0xff },    //  677  f580
	{ 0xf585,                           0x00 },    //  682  f585
	{ 0xf589,                           0x00 },    //  683  f589
	{ 0xf58a,                           0x00 },    //  684  f58a
	{ 0xf58b,                           0x00 },    //  685  f58b
	{ 0xf58c,                           0x00 },    //  686  f58c
	{ 0xf58d,                           0x00 },    //  687  f58d
	{ 0xf58e,                           0x00 },    //  688  f58e
	{ 0xf58f,                           0x00 },    //  689  f58f
	{ 0xf591,                           0x04 },    //  690  f591
	{ 0xf592,                           0x01 },    //  691  f592
	{ 0xf593,                           0x18 },    //  692  f593
	{ 0xf594,                           0x00 },    //  693  f594
	{ RSTV0910_P1_ERRCTRL1,             0x67 },    //  694  f598
	{ RSTV0910_P1_ERRCTRL2,             0xc1 },    //  698  f59c
	{ RSTV0910_P1_FECSPY,               0xa8 },    //  702  f5a0
	{ RSTV0910_P1_FSPYCFG,              0x2c },    //  703  f5a1
	{ RSTV0910_P1_FSPYDATA,             0x3a },    //  704  f5a2
	{ RSTV0910_P1_FSPYOUT,              0x07 },    //  705  f5a3
	{ RSTV0910_P1_FSPYBER,              0x11 },    //  715  f5b2
	{ RSTV0910_P1_SFERROR,              0xff },    //  716  f5c1
	{ RSTV0910_P1_SFECSTATUS,           0x44 },    //  717  f5c3
	{ RSTV0910_P1_SFKDIV12,             0x1f },    //  718  f5c4
	{ RSTV0910_P1_SFKDIV23,             0x22 },    //  719  f5c5
	{ RSTV0910_P1_SFKDIV34,             0x24 },    //  720  f5c6
	{ RSTV0910_P1_SFKDIV56,             0x24 },    //  721  f5c7
	{ RSTV0910_P1_SFKDIV67,             0x29 },    //  722  f5c8
	{ RSTV0910_P1_SFKDIV78,             0x2c },    //  723  f5c9
	{ RSTV0910_P1_SFSTATUS,             0x46 },    //  724  f5cc
	{ RSTV0910_P1_SFDLYSET2,            0x00 },    //  725  f5d0
	{ RSTV0910_P1_SFERRCTRL,            0x94 },    //  726  f5d8
	{ RSTV0910_P1_SFERRCNT2,            0x80 },    //  727  f5d9
	{ RSTV0910_P1_SFERRCNT1,            0x00 },    //  728  f5da
	{ RSTV0910_P1_SFERRCNT0,            0x00 },    //  729  f5db
	{ 0xf600,                           0x60 },    //  730  f600
	{ 0xf601,                           0x00 },    //  731  f601
	{ 0xf602,                           0x00 },    //  732  f602
	{ 0xf603,                           0x00 },    //  733  f603
	{ 0xf604,                           0x00 },    //  734  f604
	{ 0xf605,                           0x00 },    //  735  f605
	{ 0xf606,                           0x00 },    //  736  f606
	{ 0xf607,                           0xff },    //  737  f607
	{ RSTV0910_P1_DISIRQCFG,            0x00 },    //  739  f700
	{ RSTV0910_P1_DISTXCFG,             0x02 },    //  741  f702
	{ RSTV0910_P1_DISTXF22,             0xc0 },    //  745  f706
	{ RSTV0910_P1_DISTIMEOCFG,          0x02 },    //  746  f708
	{ RSTV0910_P1_DISTIMEOUT,           0x8c },    //  747  f709
	{ RSTV0910_P1_DISRXCFG,             0x34 },    //  748  f70a
	{ RSTV0910_P1_DISRXF221,            0x01 },    //  757  f714
	{ RSTV0910_P1_DISRXF220,            0x2b },    //  758  f715
	{ RSTV0910_P1_DISRXF100,            0xa9 },    //  759  f716
	{ RSTV0910_P1_DISRXSHORT22K,        0x0f },    //  760  f71c
	{ RSTV0910_P1_ACRPRESC,             0x01 },    //  761  f71e
	{ RSTV0910_P1_ACRDIV,               0x14 },    //  762  f71f
	{ 0xfa00,                           0x1c },    //  787  fa00
	{ 0xfa01,                           0x19 },    //  788  fa01
	{ 0xfa02,                           0x17 },    //  789  fa02
	{ RSTV0910_P1_NBITER_NF4,           0x18 },    //  790  fa03
	{ RSTV0910_P1_NBITER_NF5,           0x13 },    //  791  fa04
	{ RSTV0910_P1_NBITER_NF6,           0x19 },    //  792  fa05
	{ RSTV0910_P1_NBITER_NF7,           0x18 },    //  793  fa06
	{ RSTV0910_P1_NBITER_NF8,           0x17 },    //  794  fa07
	{ RSTV0910_P1_NBITER_NF9,           0x16 },    //  795  fa08
	{ RSTV0910_P1_NBITER_NF10,          0x1c },    //  796  fa09
	{ RSTV0910_P1_NBITER_NF11,          0x1c },    //  797  fa0a
	{ RSTV0910_P1_NBITER_NF12,          0x13 },    //  798  fa0b
	{ RSTV0910_P1_NBITER_NF13,          0x19 },    //  799  fa0c
	{ RSTV0910_P1_NBITER_NF14,          0x18 },    //  800  fa0d
	{ RSTV0910_P1_NBITER_NF15,          0x16 },    //  801  fa0e
	{ RSTV0910_P1_NBITER_NF16,          0x1c },    //  802  fa0f
	{ RSTV0910_P1_NBITER_NF17,          0x1c },    //  803  fa10
	{ 0xfa11,                           0x19 },    //  804  fa11
	{ 0xfa12,                           0x18 },    //  805  fa12
	{ 0xfa13,                           0x17 },    //  806  fa13
	{ 0xfa14,                           0x16 },    //  807  fa14
	{ 0xfa15,                           0x1c },    //  808  fa15
	{ 0xfa16,                           0x1c },    //  809  fa16
	{ 0xfa17,                           0x18 },    //  810  fa17
	{ 0xfa18,                           0x17 },    //  811  fa18
	{ 0xfa19,                           0x16 },    //  812  fa19
	{ 0xfa1a,                           0x1c },    //  813  fa1a
	{ 0xfa1b,                           0x1c },    //  814  fa1b
	{ 0xfa1c,                           0x1b },    //  815  fa1c
	{ 0xfa1d,                           0x19 },    //  816  fa1d
	{ 0xfa1e,                           0x17 },    //  817  fa1e
	{ 0xfa1f,                           0x1a },    //  818  fa1f
	{ 0xfa20,                           0x13 },    //  819  fa20
	{ 0xfa21,                           0x19 },    //  820  fa21
	{ 0xfa22,                           0x1b },    //  821  fa22
	{ 0xfa23,                           0x1d },    //  822  fa23
	{ 0xfa24,                           0x1b },    //  823  fa24
	{ 0xfa25,                           0x1c },    //  824  fa25
	{ 0xfa26,                           0x13 },    //  825  fa26
	{ 0xfa27,                           0x19 },    //  826  fa27
	{ 0xfa28,                           0x1b },    //  827  fa28
	{ 0xfa29,                           0x1b },    //  828  fa29
	{ 0xfa2a,                           0x1c },    //  829  fa2a
	{ 0xfa2b,                           0x19 },    //  830  fa2b
	{ 0xfa2c,                           0x1b },    //  831  fa2c
	{ 0xfa2d,                           0x1d },    //  832  fa2d
	{ 0xfa2e,                           0x1b },    //  833  fa2e
	{ 0xfa2f,                           0x1c },    //  834  fa2f
	{ 0xfa30,                           0x1b },    //  835  fa30
	{ 0xfa31,                           0x1d },    //  836  fa31
	{ 0xfa32,                           0x1b },    //  837  fa32
	{ 0xfa33,                           0x1c },    //  838  fa33
	{ RSTV0910_P1_MAXEXTRAITER,         0x07 },    //  903  fab1
	{ RSTV0910_P1_STATUSITER,           0x00 },    //  905  fabc
	{ RSTV0910_P1_STATUSMAXITER,        0x00 },    //  906  fabd
	{ 0xff40,                           0x00 },    //  968  ff40
	{ 0xff44,                           0x00 },    //  969  ff44
	{ RSTV0910_P1_TCTL4,                0x00 },    //  970  ff48
	{ 0xff57,                           0x00 },    //  971  ff57
};

static const struct stv0910_init_pair initPairs1[] = {
	{ RSTV0910_P2_I2CRPT,               0x24 },    //   17  f12b
	{ RSTV0910_P2_IQCONST,              0x00 },    //   82  f200
	{ RSTV0910_P2_NOSCFG,               0x14 },    //   83  f201
	{ RSTV0910_P2_AGC1CFG,              0x54 },    //   86  f204
	{ RSTV0910_P2_AGC1CN,               0x99 },    //   87  f206
	{ RSTV0910_P2_AGC1REF,              0x58 },    //   88  f207
	{ RSTV0910_P2_DEMOD,                0x00 },    //   97  f210
	{ RSTV0910_P2_DMDMODCOD,            0x10 },    //   98  f211
	{ RSTV0910_P2_DMDCFGMD,             0xc9 },    //  101  f214
	{ RSTV0910_P2_DMDCFG2,              0x3b },    //  102  f215
	{ RSTV0910_P2_DMDISTATE,            0x5c },    //  103  f216
	{ RSTV0910_P2_DMDT0M,               0x40 },    //  104  f217
	{ RSTV0910_P2_DMDCFG3,              0x08 },    //  108  f21e
	{ RSTV0910_P2_DMDCFG4,              0x04 },    //  109  f21f
	{ RSTV0910_P2_CORRELMANT,           0x78 },    //  110  f220
	{ RSTV0910_P2_CORRELABS,            0x80 },    //  111  f221
	{ RSTV0910_P2_CORRELEXP,            0xaa },    //  112  f222
	{ RSTV0910_P2_DMDREG,               0x01 },    //  114  f225
	{ 0xf226,                           0x00 },    //  115  f226
	{ 0xf227,                           0x00 },    //  116  f227
	{ 0xf228,                           0x00 },    //  117  f228
	{ 0xf229,                           0x00 },    //  118  f229
	{ 0xf22a,                           0x00 },    //  119  f22a
	{ 0xf22b,                           0x00 },    //  120  f22b
	{ RSTV0910_P2_AGC2O,                0x5b },    //  121  f22c
	{ RSTV0910_P2_AGC2REF,              0x38 },    //  122  f22d
	{ RSTV0910_P2_AGC1ADJ,              0x58 },    //  123  f22e
	{ 0xf22f,                           0x38 },    //  124  f22f
	{ 0xf230,                           0x38 },    //  125  f230
	{ 0xf231,                           0x38 },    //  126  f231
	{ 0xf232,                           0x38 },    //  127  f232
	{ 0xf233,                           0x38 },    //  128  f233
	{ 0xf234,                           0x47 },    //  129  f234
	{ 0xf235,                           0x38 },    //  130  f235
	{ RSTV0910_P2_AGC2I1,               0x1c },    //  131  f236
	{ RSTV0910_P2_AGC2I0,               0x74 },    //  132  f237
	{ RSTV0910_P2_CARCFG,               0x46 },    //  133  f238
	{ RSTV0910_P2_ACLC,                 0x2b },    //  134  f239
	{ RSTV0910_P2_BCLC,                 0x1a },    //  135  f23a
	{ 0xf23b,                           0x00 },    //  136  f23b
	{ 0xf23c,                           0x00 },    //  137  f23c
	{ RSTV0910_P2_CARFREQ,              0x79 },    //  138  f23d
	{ RSTV0910_P2_CARHDR,               0x1c },    //  139  f23e
	{ RSTV0910_P2_LDT,                  0xd0 },    //  140  f23f
	{ RSTV0910_P2_LDT2,                 0xb8 },    //  141  f240
	{ RSTV0910_P2_CFRICFG,              0xf9 },    //  142  f241
	{ RSTV0910_P2_CFRUP1,               0x0e },    //  143  f242
	{ RSTV0910_P2_CFRUP0,               0x69 },    //  144  f243
	{ RSTV0910_P2_CFRIBASE1,            0x01 },    //  145  f244
	{ RSTV0910_P2_CFRIBASE0,            0xf5 },    //  146  f245
	{ RSTV0910_P2_CFRLOW1,              0xf1 },    //  147  f246
	{ RSTV0910_P2_CFRLOW0,              0x97 },    //  148  f247
	{ RSTV0910_P2_CFRINIT1,             0x01 },    //  149  f248
	{ RSTV0910_P2_CFRINIT0,             0xf5 },    //  150  f249
	{ RSTV0910_P2_CFRINC1,              0x03 },    //  151  f24a
	{ RSTV0910_P2_CFRINC0,              0x8e },    //  152  f24b
	{ RSTV0910_P2_CFR2,                 0x01 },    //  153  f24c
	{ RSTV0910_P2_CFR1,                 0xf5 },    //  154  f24d
	{ RSTV0910_P2_CFR0,                 0x00 },    //  155  f24e
	{ RSTV0910_P2_TMGCFG,               0xd3 },    //  157  f250
	{ RSTV0910_P2_RTC,                  0x68 },    //  158  f251
	{ RSTV0910_P2_RTCS2,                0x55 },    //  159  f252
	{ RSTV0910_P2_TMGTHRISE,            0x08 },    //  160  f253
	{ RSTV0910_P2_TMGTHFALL,            0x08 },    //  161  f254
	{ RSTV0910_P2_SFRUPRATIO,           0x20 },    //  162  f255
	{ RSTV0910_P2_SFRLOWRATIO,          0xd0 },    //  163  f256
	{ RSTV0910_P2_KTTMG,                0xa0 },    //  164  f257
	{ RSTV0910_P2_KREFTMG,              0x80 },    //  165  f258
	{ RSTV0910_P2_SFRSTEP,              0x88 },    //  166  f259
	{ RSTV0910_P2_TMGCFG2,              0x80 },    //  167  f25a
	{ RSTV0910_P2_KREFTMG2,             0x80 },    //  168  f25b
	{ RSTV0910_P2_TMGCFG3,              0x06 },    //  169  f25d
	{ RSTV0910_P2_SFRINIT1,             0x38 },    //  170  f25e
	{ RSTV0910_P2_SFRINIT0,             0xe3 },    //  171  f25f
	{ RSTV0910_P2_SFRUP1,               0x3f },    //  172  f260
	{ RSTV0910_P2_SFRUP0,               0xff },    //  173  f261
	{ RSTV0910_P2_SFRLOW1,              0x2e },    //  174  f262
	{ RSTV0910_P2_SFRLOW0,              0x39 },    //  175  f263
	{ RSTV0910_P2_SFR3,                 0x38 },    //  176  f264
	{ RSTV0910_P2_SFR2,                 0xe3 },    //  177  f265
	{ RSTV0910_P2_SFR1,                 0x00 },    //  178  f266
	{ RSTV0910_P2_SFR0,                 0x00 },    //  179  f267
	{ RSTV0910_P2_TMGREG2,              0x00 },    //  180  f268
	{ RSTV0910_P2_TMGREG1,              0x00 },    //  181  f269
	{ RSTV0910_P2_TMGREG0,              0x00 },    //  182  f26a
	{ RSTV0910_P2_EQUALCFG,             0x41 },    //  186  f26f
	{ RSTV0910_P2_NOSCFGF1,             0x00 },    //  217  f28e
	{ 0xf28f,                           0x00 },    //  218  f28f
	{ RSTV0910_P2_CAR2CFG,              0x06 },    //  219  f290
	{ RSTV0910_P2_CFR2CFR1,             0xe5 },    //  220  f291
	{ RSTV0910_P2_CAR3CFG,              0x02 },    //  221  f292
	{ RSTV0910_P2_CFR22,                0x00 },    //  222  f293
	{ RSTV0910_P2_CFR21,                0x00 },    //  223  f294
	{ RSTV0910_P2_CFR20,                0x00 },    //  224  f295
	{ RSTV0910_P2_ACLC2S2Q,             0x0b },    //  225  f297
	{ RSTV0910_P2_ACLC2S28,             0x0a },    //  226  f298
	{ RSTV0910_P2_ACLC2S216A,           0x49 },    //  227  f299
	{ RSTV0910_P2_ACLC2S232A,           0x48 },    //  228  f29a
	{ RSTV0910_P2_BCLC2S2Q,             0x84 },    //  229  f29c
	{ RSTV0910_P2_BCLC2S28,             0x84 },    //  230  f29d
	{ 0xf29e,                           0x84 },    //  231  f29e
	{ 0xf29f,                           0x84 },    //  232  f29f
	{ RSTV0910_P2_PLROOT2,              0x00 },    //  233  f2ac
	{ RSTV0910_P2_PLROOT1,              0x00 },    //  234  f2ad
	{ RSTV0910_P2_PLROOT0,              0x01 },    //  235  f2ae
	{ RSTV0910_P2_MODCODLST0,			0xff },    //  236  f2b0
	{ RSTV0910_P2_MODCODLST1,			0xfc },    //  237  f2b1
	{ RSTV0910_P2_MODCODLST2,			0x00 },    //  238  f2b2
	{ RSTV0910_P2_MODCODLST3,			0x00 },    //  239  f2b3
	{ RSTV0910_P2_MODCODLST4,			0xc0 },    //  240  f2b4
	{ RSTV0910_P2_MODCODLST5,			0x00 },    //  241  f2b5
	{ RSTV0910_P2_MODCODLST6,			0x00 },    //  242  f2b6
	{ RSTV0910_P2_MODCODLST7,           0xc0 },    //  243  f2b7
	{ RSTV0910_P2_MODCODLST8,           0x00 },    //  244  f2b8
	{ RSTV0910_P2_MODCODLST9,           0x00 },    //  245  f2b9
	{ RSTV0910_P2_MODCODLSTA,           0xc0 },    //  246  f2ba
	{ RSTV0910_P2_MODCODLSTB,           0x00 },    //  247  f2bb
	{ RSTV0910_P2_MODCODLSTC,           0x00 },    //  248  f2bc
	{ RSTV0910_P2_MODCODLSTD,           0x00 },    //  249  f2bd
	{ RSTV0910_P2_MODCODLSTE,			0x00 },    //  250  f2be
	{ RSTV0910_P2_MODCODLSTF,			0x0f },    //  251  f2bf
	{ RSTV0910_P2_GAUSSR0,              0x98 },    //  252  f2c0
	{ RSTV0910_P2_CCIR0,                0x30 },    //  253  f2c1
	{ RSTV0910_P2_CCIQUANT,             0xac },    //  254  f2c2
	{ RSTV0910_P2_CCITHRES,             0x50 },    //  255  f2c3
	{ RSTV0910_P2_CCIACC,               0x00 },    //  256  f2c4
	{ RSTV0910_P2_DMDRESCFG,            0x29 },    //  258  f2c6
	{ RSTV0910_P2_FFECFG,               0x71 },    //  276  f2d8
	{ RSTV0910_P2_TNRCFG2,              0x02 },    //  277  f2e1
	{ RSTV0910_P2_SMAPCOEF7,            0x00 },    //  278  f300
	{ RSTV0910_P2_SMAPCOEF6,            0x00 },    //  279  f301
	{ RSTV0910_P2_SMAPCOEF5,            0x00 },    //  280  f302
	{ 0xf303,                           0x00 },    //  281  f303
	{ 0xf304,                           0x00 },    //  282  f304
	{ 0xf305,                           0x00 },    //  283  f305
	{ 0xf306,                           0x00 },    //  284  f306
	{ 0xf307,                           0x71 },    //  285  f307
	{ RSTV0910_P2_NOSTHRES1,            0x60 },    //  286  f309
	{ RSTV0910_P2_NOSTHRES2,            0x69 },    //  287  f30a
	{ RSTV0910_P2_NOSDIFF1,             0x80 },    //  288  f30b
	{ RSTV0910_P2_RAINFADE,             0x35 },    //  289  f30c
	{ RSTV0910_P2_NOSRAMCFG,            0x28 },    //  290  f30d
	{ RSTV0910_P2_NOSRAMPOS,            0x26 },    //  291  f30e
	{ RSTV0910_P2_NOSRAMVAL,            0x86 },    //  292  f30f
	{ RSTV0910_P2_VITSCALE,             0x80 },    //  298  f332
	{ RSTV0910_P2_FECM,                 0x00 },    //  299  f333
	{ RSTV0910_P2_VTH12,                0xd7 },    //  300  f334
	{ RSTV0910_P2_VTH23,                0x85 },    //  301  f335
	{ RSTV0910_P2_VTH34,                0x58 },    //  302  f336
	{ RSTV0910_P2_VTH56,                0x3a },    //  303  f337
	{ RSTV0910_P2_VTH67,                0x34 },    //  304  f338
	{ RSTV0910_P2_VTH78,                0x28 },    //  305  f339
	{ RSTV0910_P2_PRVIT,                0x2f },    //  308  f33c
	{ RSTV0910_P2_VAVSRVIT,             0x00 },    //  309  f33d
	{ RSTV0910_P2_KDIV12,               0x27 },    //  312  f340
	{ RSTV0910_P2_KDIV23,               0x32 },    //  313  f341
	{ RSTV0910_P2_KDIV34,               0x32 },    //  314  f342
	{ RSTV0910_P2_KDIV56,               0x32 },    //  315  f343
	{ RSTV0910_P2_KDIV67,               0x32 },    //  316  f344
	{ RSTV0910_P2_KDIV78,               0x50 },    //  317  f345
	{ 0xf346,                           0x00 },    //  318  f346
	{ 0xf347,                           0x00 },    //  319  f347
	{ RSTV0910_P2_PDELCTRL0,            0x01 },    //  320  f34f
	{ RSTV0910_P2_PDELCTRL1,            0x00 },    //  321  f350
	{ RSTV0910_P2_PDELCTRL2,            0x20 },    //  322  f351
	{ RSTV0910_P2_HYSTTHRESH,           0x41 },    //  323  f354
	{ RSTV0910_P2_ISIENTRY,             0x00 },    //  324  f35e
	{ RSTV0910_P2_ISIBITENA,            0x00 },    //  325  f35f
	{ RSTV0910_P2_PDELCTRL3,            0x00 },    //  341  f36f
	{ RSTV0910_P2_TSSTATEM,             0xf0 },    //  342  f370
	{ 0xf371,                           0x12 },    //  343  f371
	{ RSTV0910_P2_TSCFGH,               0x00 },    //  344  f372
	{ RSTV0910_P2_TSCFGM,               0x04 },    //  345  f373
	{ RSTV0910_P2_TSCFGL,               0x20 },    //  346  f374
	{ 0xf375,                           0x00 },    //  347  f375
	{ RSTV0910_P2_TSINSDELH,            0x00 },    //  348  f376
	{ 0xf377,                           0x00 },    //  349  f377
	{ 0xf378,                           0x00 },    //  350  f378
	{ RSTV0910_P2_TSDIVN,               0x03 },    //  351  f379
	{ RSTV0910_P2_TSCFG4,               0x00 },    //  352  f37a
	{ RSTV0910_P2_TSSPEED,              0xff },    //  353  f380
	{ 0xf385,                           0x00 },    //  358  f385
	{ 0xf389,                           0x00 },    //  359  f389
	{ 0xf38a,                           0x00 },    //  360  f38a
	{ 0xf38b,                           0x00 },    //  361  f38b
	{ 0xf38c,                           0x00 },    //  362  f38c
	{ 0xf38d,                           0x00 },    //  363  f38d
	{ 0xf38e,                           0x00 },    //  364  f38e
	{ 0xf38f,                           0x00 },    //  365  f38f
	{ 0xf391,                           0x04 },    //  366  f391
	{ 0xf392,                           0x01 },    //  367  f392
	{ 0xf393,                           0x18 },    //  368  f393
	{ 0xf394,                           0x00 },    //  369  f394
	{ RSTV0910_P2_ERRCTRL1,             0x67 },    //  370  f398
	{ RSTV0910_P2_ERRCTRL2,             0xc1 },    //  374  f39c
	{ RSTV0910_P2_FECSPY,               0xa8 },    //  378  f3a0
	{ RSTV0910_P2_FSPYCFG,              0x2c },    //  379  f3a1
	{ RSTV0910_P2_FSPYDATA,             0x3a },    //  380  f3a2
	{ RSTV0910_P2_FSPYOUT,              0x07 },    //  381  f3a3
	{ RSTV0910_P2_FSPYBER,              0x11 },    //  391  f3b2
	{ RSTV0910_P2_SFERROR,              0xff },    //  392  f3c1
	{ RSTV0910_P2_SFECSTATUS,           0x46 },    //  393  f3c3
	{ RSTV0910_P2_SFKDIV12,             0x1f },    //  394  f3c4
	{ RSTV0910_P2_SFKDIV23,             0x22 },    //  395  f3c5
	{ RSTV0910_P2_SFKDIV34,             0x24 },    //  396  f3c6
	{ RSTV0910_P2_SFKDIV56,             0x24 },    //  397  f3c7
	{ RSTV0910_P2_SFKDIV67,             0x29 },    //  398  f3c8
	{ RSTV0910_P2_SFKDIV78,             0x2c },    //  399  f3c9
	{ RSTV0910_P2_SFDLYSET2,            0x00 },    //  401  f3d0
	{ RSTV0910_P2_SFERRCTRL,            0x94 },    //  402  f3d8
	{ RSTV0910_P2_SFERRCNT2,            0x80 },    //  403  f3d9
	{ RSTV0910_P2_SFERRCNT1,            0x00 },    //  404  f3da
	{ RSTV0910_P2_SFERRCNT0,            0x00 },    //  405  f3db
	{ RSTV0910_P2_DISIRQCFG,            0x00 },    //  763  f740
	{ RSTV0910_P2_DISTXCFG,             0x02 },    //  765  f742
	{ RSTV0910_P2_DISTXF22,             0xc0 },    //  769  f746
	{ RSTV0910_P2_DISTIMEOCFG,          0x02 },    //  770  f748
	{ RSTV0910_P2_DISTIMEOUT,           0x8c },    //  771  f749
	{ RSTV0910_P2_DISRXCFG,             0x34 },    //  772  f74a
	{ RSTV0910_P2_DISRXF221,            0x01 },    //  781  f754
	{ RSTV0910_P2_DISRXF220,            0x2b },    //  782  f755
	{ RSTV0910_P2_DISRXF100,            0xa9 },    //  783  f756
	{ RSTV0910_P2_DISRXSHORT22K,        0x0f },    //  784  f75c
	{ RSTV0910_P2_ACRPRESC,             0x01 },    //  785  f75e
	{ RSTV0910_P2_ACRDIV,               0x14 },    //  786  f75f
	{ RSTV0910_P2_MAXEXTRAITER,         0x07 },    //  904  fab6
	{ RSTV0910_P2_STATUSITER,           0x00 },    //  907  fabe
	{ RSTV0910_P2_STATUSMAXITER,        0x00 },    //  908  fabf
	{ 0xfac0,                           0x1c },    //  909  fac0
	{ 0xfac1,                           0x19 },    //  910  fac1
	{ 0xfac2,                           0x17 },    //  911  fac2
	{ RSTV0910_P2_NBITER_NF4,           0x18 },    //  912  fac3
	{ RSTV0910_P2_NBITER_NF5,           0x13 },    //  913  fac4
	{ RSTV0910_P2_NBITER_NF6,           0x19 },    //  914  fac5
	{ RSTV0910_P2_NBITER_NF7,           0x18 },    //  915  fac6
	{ RSTV0910_P2_NBITER_NF8,           0x17 },    //  916  fac7
	{ RSTV0910_P2_NBITER_NF9,           0x16 },    //  917  fac8
	{ RSTV0910_P2_NBITER_NF10,          0x1c },    //  918  fac9
	{ RSTV0910_P2_NBITER_NF11,          0x1c },    //  919  faca
	{ RSTV0910_P2_NBITER_NF12,          0x13 },    //  920  facb
	{ RSTV0910_P2_NBITER_NF13,          0x19 },    //  921  facc
	{ RSTV0910_P2_NBITER_NF14,          0x18 },    //  922  facd
	{ RSTV0910_P2_NBITER_NF15,          0x16 },    //  923  face
	{ RSTV0910_P2_NBITER_NF16,          0x1c },    //  924  facf
	{ RSTV0910_P2_NBITER_NF17,          0x1c },    //  925  fad0
	{ 0xfad1,                           0x19 },    //  926  fad1
	{ 0xfad2,                           0x18 },    //  927  fad2
	{ 0xfad3,                           0x17 },    //  928  fad3
	{ 0xfad4,                           0x16 },    //  929  fad4
	{ 0xfad5,                           0x1c },    //  930  fad5
	{ 0xfad6,                           0x1c },    //  931  fad6
	{ 0xfad7,                           0x18 },    //  932  fad7
	{ 0xfad8,                           0x17 },    //  933  fad8
	{ 0xfad9,                           0x16 },    //  934  fad9
	{ 0xfada,                           0x1c },    //  935  fada
	{ 0xfadb,                           0x1c },    //  936  fadb
	{ 0xfadc,                           0x1b },    //  937  fadc
	{ 0xfadd,                           0x19 },    //  938  fadd
	{ 0xfade,                           0x17 },    //  939  fade
	{ 0xfadf,                           0x1a },    //  940  fadf
	{ 0xfae0,                           0x13 },    //  941  fae0
	{ 0xfae1,                           0x19 },    //  942  fae1
	{ 0xfae2,                           0x1b },    //  943  fae2
	{ 0xfae3,                           0x1d },    //  944  fae3
	{ 0xfae4,                           0x1b },    //  945  fae4
	{ 0xfae5,                           0x1c },    //  946  fae5
	{ 0xfae6,                           0x13 },    //  947  fae6
	{ 0xfae7,                           0x19 },    //  948  fae7
	{ 0xfae8,                           0x1b },    //  949  fae8
	{ 0xfae9,                           0x1b },    //  950  fae9
	{ 0xfaea,                           0x1c },    //  951  faea
	{ 0xfaeb,                           0x19 },    //  952  faeb
	{ 0xfaec,                           0x1b },    //  953  faec
	{ 0xfaed,                           0x1d },    //  954  faed
	{ 0xfaee,                           0x1b },    //  955  faee
	{ 0xfaef,                           0x1c },    //  956  faef
	{ 0xfaf0,                           0x1b },    //  957  faf0
	{ 0xfaf1,                           0x1d },    //  958  faf1
	{ 0xfaf2,                           0x1b },    //  959  faf2
	{ 0xfaf3,                           0x1c },    //  960  faf3
	{ 0xff20,                           0x00 },    //  964  ff20
	{ 0xff24,                           0x00 },    //  965  ff24
	{ RSTV0910_P2_TCTL4,                0x00 },    //  966  ff28
	{ 0xff37,                           0x00 },    //  967  ff37
};

STV0910_INIT_TABLE(demodInit,  initPairs,  0);
STV0910_INIT_TABLE(demodInit1, initPairs1, &demodInit);
STV0910_INIT_TABLE(demodInit0, initPairs0, &demodInit1);

#endif