/*
 * TurboSight TBS PCIE DVB driver
 *
 * Copyright (C) 2014 Konstantin Dimitrov <kosio.dimitrov@gmail.com>
 * Copyright (C) 2014 www.tbsdtv.com
 * Copyright (C) 2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _TBS_PCIE_REG_H
#define _TBS_PCIE_REG_H

#define TBS_GPIO_BASE		0x0000

#define TBS_GPIO_DATA(i)	((3 - (i)) * 0x04)
#define TBS_GPIO_DATA_3		0x00	/* adapter 0 */
#define TBS_GPIO_DATA_2		0x04	/* adapter 1 */
#define TBS_GPIO_DATA_1		0x08	/* adapter 2 */
#define TBS_GPIO_DATA_0		0x0c	/* adapter 3 */

#define TBS_VOLTAGE_OFF		0x4
#define TBS_VOLTAGE_18		0x2
#define TBS_RESET			0x1

#define	TBS_ID_BASE			0x0020
#define TBS_MODEL			0x0
#define TBS_UNKNOWN0		0x4

#define TBS_I2C_BASE_3		0x4000
#define TBS_I2C_BASE_2		0x5000
#define TBS_I2C_BASE_1		0x6000
#define TBS_I2C_BASE_0		0x7000

#define TBS_I2C_CTRL		0x00
#define TBS_I2C_DATA		0x04

#define TBS_I2C_START_BIT	(0x00000001 <<  7)
#define TBS_I2C_STOP_BIT	(0x00000001 <<  6)

#define TBS_I2C_SADDR_2BYTE	(0x00000001 <<  5)
#define TBS_I2C_SADDR_1BYTE	(0x00000001 <<  4)

#define TBS_I2C_READ_BIT	(0x00000001 <<  8)

#define TBS_INT_BASE		0xc000

#define TBS_INT_STATUS		0x00
#define TBS_INT_ENABLE		0x04
#define TBS_I2C_MASK_3		0x08
#define TBS_I2C_MASK_2		0x0c
#define TBS_I2C_MASK_1		0x10
#define TBS_I2C_MASK_0		0x14

#define TBS_DMA_MASK(i)		(0x18 + (3 - (i)) * 0x04)
#define TBS_DMA_MASK_3		0x18
#define TBS_DMA_MASK_2		0x1C
#define TBS_DMA_MASK_1		0x20
#define TBS_DMA_MASK_0		0x24

#define TBS_DMA2_MASK(i)	(0x28 + (7 - (i)) * 0x04)
#define TBS_DMA2_MASK_3		0x28
#define TBS_DMA2_MASK_2		0x2C
#define TBS_DMA2_MASK_1		0x30
#define TBS_DMA2_MASK_0		0x34

#define TBS_DMA_BASE(i)		(0x8000 + (3 - (i)) * 0x1000)
#define TBS_DMA_BASE_3		0x8000
#define TBS_DMA_BASE_2		0x9000
#define TBS_DMA_BASE_1		0xa000
#define TBS_DMA_BASE_0		0xb000

#define TBS_DMA2_BASE(i)	(0x8800 + (7 - (i)) * 0x1000)
#define TBS_DMA2_BASE_3		0x8800
#define TBS_DMA2_BASE_2		0x9800
#define TBS_DMA2_BASE_1		0xa800
#define TBS_DMA2_BASE_0		0xb800

#define TBS_DMA_START		0x00
#define TBS_DMA_STATUS		0x00
#define TBS_DMA_SIZE		0x04
#define TBS_DMA_ADDR_HIGH	0x08
#define TBS_DMA_ADDR_LOW	0x0c
#define TBS_DMA_CELL_SIZE	0x10

#define TBS_PCIE_PAGE_SIZE	4194304
#define TBS_PCIE_DMA_TOTAL	385024
#define TBS_PCIE_CELL_SIZE	48128

#endif
